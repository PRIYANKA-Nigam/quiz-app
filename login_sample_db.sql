-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2023 at 11:56 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login_sample_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `aid` int(250) NOT NULL,
  `answer` varchar(250) DEFAULT NULL,
  `ans_id` int(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`aid`, `answer`, `ans_id`) VALUES
(1, 'True', 1),
(2, 'False', 1),
(3, 'Can\'t say', 1),
(4, 'May be', 1),
(5, '2', 2),
(6, '12', 2),
(7, '3', 2),
(8, '5', 2),
(9, '.container-fixed', 3),
(10, '.container-fluid', 3),
(11, '.container', 3),
(12, 'All of the above', 3),
(13, '.dropdown', 4),
(14, '.select', 4),
(15, '.select-list', 4),
(16, 'None of the above', 4),
(17, '.img-rounded', 5),
(18, '.img-circle', 5),
(19, '.img-rounded', 5),
(20, 'None of the above', 5),
(21, '.btn-xl', 6),
(22, '.btn-lrg', 6),
(23, '.btn-large', 6),
(24, '.btn-lg', 6),
(25, 'popup', 7),
(26, 'alert', 7),
(27, 'modal', 7),
(28, 'window', 7),
(29, '.page', 8),
(30, '.pagin', 8),
(31, '.paginate', 8),
(32, '.pagination', 8);

-- --------------------------------------------------------

--
-- Table structure for table `loggeduser`
--

CREATE TABLE `loggeduser` (
  `uid` int(250) NOT NULL,
  `username` varchar(250) DEFAULT NULL,
  `totalques` int(250) DEFAULT NULL,
  `answerscorrect` int(250) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loggeduser`
--

INSERT INTO `loggeduser` (`uid`, `username`, `totalques`, `answerscorrect`, `status`) VALUES
(1, 'z', 8, 6, 'Pass ...'),
(88, 'priya', 8, 4, 'SatisFactory ... '),
(92, 'pppy', 8, 0, 'Fail'),
(93, 'b', 8, 2, 'Fail');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `qid` int(250) NOT NULL,
  `question` varchar(250) DEFAULT NULL,
  `ans_id` int(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`qid`, `question`, `ans_id`) VALUES
(1, 'Is Bootstrap3 mobile-first? ', 1),
(2, 'How many columns are allowed in a bootstrap grid system?', 6),
(3, 'Which of the following class in Bootstrap is used to provide a responsive fixed width container?', 11),
(4, 'Which of the following class in Bootstrap is used to create a dropdown menu?', 13),
(5, 'The class used to shape an image to a circle is -', 18),
(6, 'Which of the following class in Bootstrap is used to create a large button?', 24),
(7, 'Which of the following plugin in Bootstrap is used to create a modal window?', 27),
(8, 'Which of the following class in Bootstrap is used to create basic pagination?', 32);

-- --------------------------------------------------------

--
-- Table structure for table `quizgame`
--

CREATE TABLE `quizgame` (
  `qid` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `choice1` varchar(255) NOT NULL,
  `choice2` varchar(255) NOT NULL,
  `choice3` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quizgame`
--

INSERT INTO `quizgame` (`qid`, `question`, `choice1`, `choice2`, `choice3`, `answer`) VALUES
(1, 'What is codeigniter?', ' Java web application framework', 'JS Framework', 'Python web application framework', 'PHP web application framework'),
(2, 'Which one is the business logic in codeigniter?', 'View', 'Config', 'Controller', 'Model'),
(3, 'CodeIgniter is written in ...................... language.', 'Java', 'Python', 'C', 'PHP'),
(4, 'Which of the following function is use to set the value of form input box ?', 'set()', 'setvalue()', 'None of the mention', 'set_value()'),
(5, 'Which folder will contain base class of your application?', 'Logs', 'Config', 'Models', 'Core');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `image_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_name`, `password`, `date`, `image_name`) VALUES
(1, 4987, 'priya', '1234', '2022-09-04 12:10:13', ''),
(2, 4456481643, 'chiya', '12345', '2022-09-04 12:11:10', ''),
(6, 1255997456039, 'pp', 'nn', '2023-03-18 13:21:45', 'data privecy certificate.PNG'),
(7, 9223372036854775807, 'a', 'b', '2023-03-19 13:29:03', 'upload/'),
(8, 89780391698, 'b', 'c', '2023-03-30 12:00:06', 'upload/Priyanka.jpg'),
(9, 6547, 'priya', 'pa', '2023-03-19 07:20:45', 'upload/php.png'),
(10, 9378887, 'pppy', 'nnn', '2023-03-19 12:21:45', 'upload/Capture6.PNG'),
(12, 814836, 'z', 'x', '2023-03-19 08:56:11', 'upload/rest.jpeg'),
(13, 14324621652298730, 'tina dabi', '1a23', '2023-03-20 06:16:03', 'upload/certi human error.PNG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `loggeduser`
--
ALTER TABLE `loggeduser`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `quizgame`
--
ALTER TABLE `quizgame`
  ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `date` (`date`),
  ADD KEY `user_name` (`user_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `aid` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `loggeduser`
--
ALTER TABLE `loggeduser`
  MODIFY `uid` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `qid` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `quizgame`
--
ALTER TABLE `quizgame`
  MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
